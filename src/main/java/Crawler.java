import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Crawler extends SimpleFileVisitor<Path> {

    private static final String ETALIS_ENTRY =
            "(?i)<classpathentry.*?/sonata-libs/source/lib/managed/etalis/((etalis|origo|ergo|ctc){1}[\\w-]+).jar.*?/>";
    private static final String ATTR_EXPORTED = "exported";
    private static final String ATTR_SRC = "path";

    private final boolean createBackup;
    private final String replacedSuffix;

    public Crawler(final boolean createBackup, final String replacedSuffix) {
        this.createBackup = createBackup;
        this.replacedSuffix = replacedSuffix;
    }

    @Override
    public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {

        final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:**/.classpath");

        if (pathMatcher.matches(file)) {

            if (createBackup) {
                TextFileUtils.createInputBackup(file);
            }

            final String fileContent = TextFileUtils.readFileContent(file);
            final String instrumentedFileContent = instrumentFile(fileContent);
            final Path outputPath = Paths.get(file.getParent().toString(), ".classpath" + replacedSuffix);
            TextFileUtils.writeFileContent(outputPath, instrumentedFileContent);
        }

        return FileVisitResult.CONTINUE;
    }

    private String instrumentFile(final String fileContent) throws IOException {
        String instrumentedFileContent = fileContent;

        do {
            final Pattern pattern = Pattern.compile(ETALIS_ENTRY);
            final Matcher matcher = pattern.matcher(instrumentedFileContent);

            if (matcher.find()) {
                final Document xmlNode = convertToXml(matcher.group());
                final String newEntry = instrumentEntry(xmlNode, matcher.group(1));
                instrumentedFileContent = matcher.replaceFirst(newEntry);
            } else {
                break;
            }
        } while (true);

        return instrumentedFileContent;
    }

    private String instrumentEntry(final Document xmlNode, final String moduleName) {

        if (xmlNode.getChildNodes().getLength() != 1) {
            throw new IllegalArgumentException("Unexpected Input");
        }

        final StringBuilder sb = new StringBuilder();
        sb.append("<classpathentry ").append("combineaccessrules=\"false\" ").append("kind=\"src\" ");

        final NamedNodeMap attributes = xmlNode.getChildNodes().item(0).getAttributes();

        for (int i = 0; i < attributes.getLength(); i++) {
            final Node attribute = attributes.item(i);

            switch (attribute.getNodeName().toLowerCase()) {
                case ATTR_EXPORTED:
                    sb.append(attribute.getNodeName().toLowerCase()).append("=\"").append(attribute.getNodeValue())
                            .append("\" ");

            }

        }

        sb.append(ATTR_SRC).append("=\"/").append(moduleName).append("\"/>");
        return sb.toString();
    }

    private Document convertToXml(final String entry) throws IOException {
        Document document = null;

        try {
            final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = dbf.newDocumentBuilder();
            final Reader stringReader = new StringReader(entry);
            final InputSource inputSource = new InputSource(stringReader);
            document = builder.parse(inputSource);
        } catch (ParserConfigurationException | SAXException e) {
            throw new IOException(e);
        }

        return document;
    }
}

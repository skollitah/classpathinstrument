import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Instrument {

    public static void main(final String[] args) throws IOException {

        Path file = null;
        boolean createBackup = true;

        switch (args.length) {
            case 2:
                createBackup = Boolean.valueOf(args[1]);
            case 1:
                file = Paths.get(args[0]);
                break;
            default:
                System.out.println("Please provide entry directory");
        }

        Files.walkFileTree(file, new Crawler(createBackup, ""));
    }
}

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;

public class TextFileUtils {


    public static void createInputBackup(final Path file) throws IOException {
        final Path backupPath = Paths.get(file.getParent().toString(), file.getFileName() + ".backup");
        Files.copy(file, backupPath, StandardCopyOption.REPLACE_EXISTING);

    }

    public static void writeFileContent(final Path file, final String fileContent) throws IOException {
        Files.write(file, fileContent.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    public static String readFileContent(final Path file) throws IOException {
        final byte[] fileBytes = Files.readAllBytes(file);
        return new String(fileBytes, Charset.forName("UTF-8"));
    }
}


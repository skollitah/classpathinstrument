import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class CrawlerTest extends TestBase {

    @Test
    public void shouldInstrumentClassPathWithBackup() throws IOException {
        // given
        final Path entryPoint = getResourcePath("dir");
        final String cmpClasspath1Content = TextFileUtils.readFileContent(classpath1Cmp);
        final String cmpClasspath2Content = TextFileUtils.readFileContent(classpath2Cmp);

        // when
        Files.walkFileTree(entryPoint, new Crawler(true, ""));
        final String newClasspath1Content = TextFileUtils.readFileContent(classpath1);
        final String newClasspath2Content = TextFileUtils.readFileContent(classpath2);


        //
        assertThat(Files.exists(classpath1backup));
        assertThat(Files.exists(classpath2backup));
        assertThat(newClasspath1Content).isEqualTo(cmpClasspath1Content);
        assertThat(newClasspath2Content).isEqualTo(cmpClasspath2Content);
    }
}

import org.junit.After;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class TestBase {

    final protected Path entryPoint = getResourcePath("dir");
    final protected Path classpath1 = Paths.get(entryPoint.toString(), "dir1", ".classpath");
    final protected Path classpath1Cmp = Paths.get(entryPoint.toString(), "dir1", ".compare");
    final protected Path classpath1backup = Paths.get(entryPoint.toString(), "dir1", ".classpath.backup");
    final protected Path classpath2 = Paths.get(entryPoint.toString(), "dir2", ".classpath");
    final protected Path classpath2backup = Paths.get(entryPoint.toString(), "dir2", ".classpath.backup");
    final protected Path classpath2Cmp = Paths.get(entryPoint.toString(), "dir2", ".compare");


    protected Path getResourcePath(final String resource) {
        final URL url = TestBase.class.getClassLoader().getResource(resource);
        try {
            return Paths.get(url.toURI());
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @After
    public void tearDown() throws IOException {
        clean();
    }

    private void clean() throws IOException {

        if (Files.exists(classpath1)) {
            Files.copy(classpath1backup, classpath1, StandardCopyOption.REPLACE_EXISTING);
            Files.delete(classpath1backup);
        }

        if (Files.exists(classpath2)) {
            Files.copy(classpath2backup, classpath2, StandardCopyOption.REPLACE_EXISTING);
            Files.deleteIfExists(classpath2backup);
        }

    }
}
